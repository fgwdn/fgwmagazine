import pyrebase


config = {
    "apiKey": "AIzaSyBTJ0B6Nh-bcsGFgVmBqKE15RwrlYXLF7M",
    "authDomain": "comp1640-976c9.firebaseapp.com",
    "databaseURL": "https://comp1640-976c9-default-rtdb.firebaseio.com",
    "projectId": "comp1640-976c9",
    "storageBucket": "comp1640-976c9.appspot.com",
    "serviceAccount" : "serviceAccountKey.json"
}

firebase_storage = pyrebase.initialize_app(config)
storage = firebase_storage.storage()

path = "COMP1787-RequirementsManagemenGuidelines-Detail.docx"
storage.child("articles/COMP1787 - Requirements Management Guidelines - Detail.docx").download("static/tmp/" + path)


allfiles = storage.list_files()

for file in allfiles:
    print(file.name)
    # file.download_to_filename(file.name)

# import os
   
# filelist = [f for f in os.listdir(".") if f.endswith(".jpg")]
# for f in filelist:
#     print(os.path.join(".",f))