export class Contribution {
    id?: any;
    user?: any;
    faculty?: any;
    file_type?: string;
    title?: string;
    note?: string;
    contribution_path?: any;
    upload_time?: any;
    vote?: number;
    check_selected?: boolean;
}
